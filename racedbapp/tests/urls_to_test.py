test_urls = (
    "/",
    "/admin/login/",
    "/boost/2024/",
    "/distance/5-km/",
    "/endurrun/",
    "/endurrun/ultimate/",
    "/event/2023/fall-classic/5-km/",
    "/events/",
    "/medals/2023/fall-classic/5-km/",  # standard
    "/medals/2023/waterloo-classic/10-km/",  # classic-5oa
    "/members/",
    "/multiwins/",
    "/race/fall-classic/5-km/",
)
